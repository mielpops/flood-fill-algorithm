# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    main.py                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: bbellavi <bbellavi@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/05/19 19:37:30 by bbellavi          #+#    #+#              #
#    Updated: 2020/05/24 18:33:27 by bbellavi         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

import os
import time

OKGREEN = '\033[92m'
ENDC	= '\033[0m'

with open("map3", "r") as f:
	map = [list(line.strip("\n")) for line in f.readlines()]

def print_map(map, target_color):
	"""
		Print the map with target color, colored by specific color.

		:param map: The map, formatted with 1 and 0
		:type map: list[str]
		:returns: Nothing
		:rtype: None
	"""
	for line in map:
		print("\t", end="")
		for char in line:
			if char == target_color:
				print(f"{OKGREEN}{char}{ENDC}", end="")
			else:
				print(char, end="")
		print()


def get_start_point(map):
	"""
		Get the starting point of the pixel, represented by N.
		
		:param map: The map, formatted with 1 and 0
		:type map: list[str]
		:returns: A tuple representing the starting point, otherwise None
		:rtype: tuple
	"""
	for y, line in enumerate(map):
		for x, char in enumerate(line):
			if char == "N":
				return x, y

def flood_fill(map, x, y, target_color, replace_color):
	"""
		Flood fill algorithm recursive implementation.
		The algorithm will check for target color of
		adjacent pixels of the current pixel beeing
		processed by flood fill.

		:param map: The map, formatted with 1 and 0
		:type map: list[str]
		:param x: int representing the x position of start pixel
		:type x: int
		:param y: int representing the y position of start pixel
		:type y: int
		:param target_color: The target color that will be replaced by replace_color
		:type target_color: str
		:param replace_color: The color that will replace target_color
		:type replace_color: str
		:returns: True if the map is closed otherwise False
		:rtype: bool
	"""
	wall = '1'

	if map[y][x] == wall or map[y][x] == replace_color:
		return

	if map[y][x] == target_color or map[y][x] == 'N':
		map[y][x] = replace_color
		flood_fill(map, x, y - 1, target_color, replace_color)
		flood_fill(map, x, y + 1, target_color, replace_color)
		flood_fill(map, x - 1, y, target_color, replace_color)
		flood_fill(map, x + 1, y, target_color, replace_color)
		
	return

def flood_fill_stack(map, pixel, target_color, replace_color):
	"""
		Flood fill algorithm implementation with stack.
		The algorithm will check for target color of
		adjacent pixels of the current pixel beeing
		processed by flood fill.

		:param map: The map, formatted with 1 and 0
		:type map: list[str]
		:param pixel: Tuple representing the start pixel
		:type pixel: tuple
		:param target_color: The target color that will be replaced by replace_color
		:type target_color: str
		:param replace_color: The color that will replace target_color
		:type replace_color: str
		:returns: True if the map is closed otherwise False
		:rtype: bool
	"""
	dx = [0, 1, 0, -1]
	dy = [-1, 0, 1, 0]
	stack = []
	x, y = pixel

	if map[y][x] != target_color and map[y][x] != "N":
		return
		
	stack.append(pixel)
	xmax = len(map[0])
	ymax = len(map)
	while len(stack) != 0:
		x, y = stack.pop()
		map[y][x] = replace_color

		for x1, y1 in zip(dx, dy):
			nx = x + x1
			ny = y + y1
			os.system("clear")
			print_map(map, replace_color)
			time.sleep(0.2)
			if nx >= 0 and nx < xmax and ny >= 0 and ny < ymax:
				if map[ny][nx] == target_color:
					stack.append((nx, ny))
			else:
				return False
	
	return True

def get_max_len(map):
	"""
		Get the maximum line length into the map.
		It is useful when pre-processing the map.

		:param map: The map, formatted with 1 and 0
		:type map: list[str]
		:returns: The maximum line length of the map
		:rtype: int
	"""
	maxlen = len(map[0])
	curlen = 0

	for line in map:
		curlen = len(line)
		maxlen = max(maxlen, curlen)
	
	return maxlen

def fill_empty(map):
	"""
		Fill empty pre-process the map for flood-fill.
		It will replace spaces characters at the beginning
		and the end of the line.

		:param map: The map to pre-process, formatted with 1 and 0
		:type map: list[str]
		:returns: Nothing
		:rtype: None
	"""
	maxlen = get_max_len(map)

	for line in map:
		curlen = len(line)
		for i in range(curlen):
			if line[i] == " ":
				line[i] = "0"

		if curlen < maxlen:
			fill_len = maxlen - curlen
			for _ in range(fill_len):
				line.append("0")

	for line in map:
		print("".join(line))


x, y = get_start_point(map)
fill_empty(map)
is_closed = flood_fill_stack(map, (x, y), "0", "x")

if is_closed:
	print("\tMap is closed!")
else:
	print("\tMap isn't closed!")